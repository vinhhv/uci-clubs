//
//  ClubsQuery.swift
//  My Clubs
//
//  Created by Vinh Vu on 9/7/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import Foundation
import Parse

class ClubsQuery
{
    
    // MARK: - General Club Fetching Queries
    
    /**
        Fetches Parse club objects given a club category name and the Clubs table view.
     
        - Parameters:
            
            - clubCategory: The name of the category of clubs, ex. Business, Sports, etc.
     
            - sender: The ClubsTableViewController which requested the query. The sender's 'clubsList' variable ([String : (String, String)]) will be updated with the results of the query.
    */
    static func getClubsByCategory(clubCategory clubCategory: String, sender: ClubsTableViewController)
    {
        let clubQuery = PFQuery(className: "Club")
        clubQuery.whereKey("category", equalTo: clubCategory)
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)) { () -> Void in
            do
            {
                let clubObjects = try clubQuery.findObjects()
                
                for clubObject in clubObjects
                {
                    guard let club = clubObject["name"] as? String else
                    {
                        throw Alert.QueryError.ParseToNativeConversionError
                    }
                    dispatch_async(dispatch_get_main_queue()) { () -> Void in
                        if let clubId = clubObject.objectId
                        {
                            sender.clubsList[clubId] = (club, clubId)
                        }
                    }
                }
            }
            catch
            {
                Alert.displayAlert(title: Alert.ErrorMessages.Failed, message: Alert.ErrorMessages.ClubsQuery, viewController: sender, dismiss: true)
            }
        }
    }
    
    
    /**
        Fetches Parse club categories.

        - Parameters:

            - sender: The ClubsCategoriesTableViewController which requested the query. The sender's 'clubCategories' variable ([String]) will be updated with the results of the query.
     */
    static func getClubCategories(sender: ClubCategoriesTableViewController)
    {
        let clubCategoryQuery = PFQuery(className: "ClubCategories")
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)) { () -> Void in
            do
            {
                let clubCategoriesClass = try clubCategoryQuery.getFirstObject()
                guard let clubCategories: [String] = clubCategoriesClass["Categories"] as? Array else
                {
                    throw Alert.QueryError.ParseToNativeConversionError
                }
                
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    for club in clubCategories
                    {
                        sender.clubCategories.append(club)
                    }
                    sender.clubCategories.sortInPlace {$0 < $1}
                }
                
            }
            catch
            {
                Alert.displayAlert(title: Alert.ErrorMessages.Failed, message: Alert.ErrorMessages.ClubsQuery, viewController: sender, dismiss: false)
            }
        }
    }
    
    // MARK: - Club Attribute Queries
    
    /**
        Fetches Parse club categories.

        - Parameters:

            - clubId: The unique Parse ID given to this club.
    
            - sender: The ClubMembersTableViewController which requested the query. The sender's 'clubMembers' variable ([String]) will be updated with the results of the query.
    */
    static func getMembers(clubId clubId: String, sender: ClubMembersTableViewController)
    {
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)) { () -> Void in
            do
            {
                let club = try PFQuery.getObjectOfClass("Club", objectId: clubId)
                let membersRelation = club.relationForKey("members")
                let membersQuery = try membersRelation.query()?.findObjects()
                var members = [String]()
                
                guard let membersList = membersQuery else
                {
                    throw Alert.QueryError.ParseOptionalUnwrapError
                }
                
                for member in membersList
                {
                    if let member: AnyObject = member as AnyObject!
                    {
                        members.append(member.username as String!)
                    }
                }
                members.sortInPlace({$0 < $1})
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    sender.clubMembers = members
                }
            }
            catch
            {
                Alert.displayAlert(title: "Failed", message: Alert.ErrorMessages.MembersQuery, viewController: sender, dismiss: false)
            }
        }
    }

}