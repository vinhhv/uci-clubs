//
//  AnnouncementsQuery.swift
//  My Clubs
//
//  Created by Vinh Vu on 9/16/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import Foundation
import Parse

class AnnouncementsQuery
{
    
    // MARK: - General Announcements Queries
    
    /**
        Fetches the Parse Announcement objects given a unique club ID.
    
        - Parameters:
        
            - clubId: The unique Parse ID given to this club.
    
            - sender: The AnnouncementsTableViewController which requested the query. The sender's announcements variable ([PFObject]) will be updated with the results.
    
            - refresh: The UIRefreshControl object which will be called to end refreshing when the query results are delivered.
    */
    static func getAnnouncements(clubId clubId: String, sender: AnnouncementsTableViewController, refresh: UIRefreshControl?)
    {
        let clubQuery = PFQuery(className: "Club")
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0), { () -> Void in
            do
            {
                let club = try clubQuery.getObjectWithId(clubId)
                let announcementRelation = club.relationForKey("announcements")
                let announcementsOptionalObjects = try announcementRelation.query()?.orderByDescending("updatedAt").findObjects()
                
                guard let announcementUnwrappedObjects = announcementsOptionalObjects
                else
                {
                    throw Alert.QueryError.ParseOptionalUnwrapError
                }
                
                var announcements = [PFObject]()
                for announcement in announcementUnwrappedObjects
                {
                    announcements.append(announcement)
                }
                
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    sender.announcements = announcements
                    refresh?.endRefreshing()
                }
            }
            catch
            {
                Alert.displayAlert(title: Alert.ErrorMessages.Failed, message: Alert.ErrorMessages.AnnouncementsQuery, viewController: sender, dismiss: false)
            }
        })
    }
    
    /**
        Fetches all announcements belonging to the club(s) the current user is following. Announcements will be appear in last updated order, from newest to oldest.
     
        - Parameters:
     
            - sender: The NewsFeedTableViewController which requested the result. The sender's announcements variable ([PFObject]) will be updated with the results.
     
            - refresh: The UIRefreshControl object which will be called to end refreshing when the query results are delivered.
    */
    static func getUserNewsFeed(sender sender: NewsFeedTableViewController, refresh: UIRefreshControl?)
    {
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)) { () -> Void in
            guard let user = PFUser.currentUser() else
            {
                Alert.displayAlert(title: Alert.ErrorMessages.Failed, message: Alert.ErrorMessages.NewsFeedQuery, viewController: sender, dismiss: false)
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    refresh?.endRefreshing()
                }
                return
            }
            
            let clubs = user.relationForKey("clubs")
            do
            {
                let clubObjects = try clubs.query()?.findObjects()
                
                guard let clubs = clubObjects else
                {
                    throw Alert.QueryError.ParseOptionalUnwrapError
                }
                
                sender.followingList = [String]()
                var anmnts = [PFObject]()
                
                for club in clubs
                {
                    sender.followingList!.append(club.objectId as String!)
                    let announcementRelation = club.relationForKey("announcements")
                    let clubName = club["name"] as! String
                    let clubId = club.objectId as String!
                    
                    let announcementObjects = try announcementRelation.query()?.findObjects()
                    
                    guard let announcements = announcementObjects else
                    {
                        throw Alert.QueryError.ParseOptionalUnwrapError
                    }
                    
                    for announcement in announcements
                    {
                        announcement["clubName"] = clubName
                        announcement["clubId"] = clubId
                        anmnts.append(announcement)
                    }
                }
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    sender.announcements = anmnts
                    refresh?.endRefreshing()
                }
                
            }
            catch
            {
                Alert.displayAlert(title: Alert.ErrorMessages.Failed, message: Alert.ErrorMessages.NewsFeedQuery, viewController: sender, dismiss: false)
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    refresh?.endRefreshing()
                }
            }
        }
    }
}
