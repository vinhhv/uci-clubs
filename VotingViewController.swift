//
//  VotingViewController.swift
//  My Clubs
//
//  Created by Vinh Vu on 9/13/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import UIKit
import Parse

class VotingViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var questionTextView: UITextView!
    @IBOutlet weak var resultsTextView: UITextView!
    @IBOutlet weak var votingPickerView: UIPickerView!
    @IBOutlet weak var submitButton: UIButton!
    
    @IBAction func submitVote() {
        if !alreadyVoted
        {
            QuestionsQuery.updateVote(questionObject: question, selectedRow: selectedRow, sender: self, addVote: true)
        }
        else
        {
            QuestionsQuery.updateVote(questionObject: question, selectedRow: currentVote!, sender: self, addVote: false)
        }
    }
    
    @IBAction func refreshResults() {
        QuestionsQuery.getQuestionById(questionId: question?.objectId as String!, sender: self)
    }
    
    // Must be set since initial view of UIPickerView will not recognize default value
    var selectedRow: Int = 0
    
    // Voting Status of the user for the current question
    var alreadyVoted = false
    
    // Current selected vote of the user, NULL if has not voted
    var currentVote: Int?
    
    /**
        Following status of user to club
     
        This ensures only members of the club are able to participate
        in the voting process.
     */
    var following: Bool?
    
    /**
        Parse Relation indicating which set of Question objects
        in the Parse database belongs to the current club.

        This allows the application to quickly pull in the correct
        subset of Question objects without searching through the
        entire database.
     */
    var questionsRelation: PFRelation?
    
    // The parent ClubTabBarController in which this view controller currently resides in
    var tabBar: ClubTabBarController?
    {
        didSet
        {
            following = tabBar!.following
        }
    }
    
    /**
        The current voting status for the user (initialized in VoteQuery.swift)

        
        Status Info:
            
            NoUser          - No user is currently logged in: the submit vote button is disabled
            
            Error           - An error occurred: the submit vote button is disabled
     
            NotAlreadyVoted - User is logged in and has not submitted a vote: submit vote button is enabled
     
            AlreadyVoted    - User is logged in and has already submitted a vote: remove vote button is enabled
    */
    var voteStatus: QuestionsQuery.VoteStatus?
    {
        didSet
        {
            switch(voteStatus!.voteStatus)
            {
                case .NoUser:
                    submitButton.enabled = false
                    break
                case .Error:
                    submitButton.enabled = false
                    break
                case .NotAlreadyVoted:
                    alreadyVoted = false
                    submitButton.setTitle("Submit", forState: .Normal)
                    break
                case .AlreadyVoted:
                    alreadyVoted = true
                    currentVote = voteStatus!.votedFor
                    submitButton.setTitle("Remove", forState: .Normal)
            }
            refresh()
            
        }
    }
    
    /**
        Refreshes the current question's information
     
        Vote count for each possible answer is updated and refreshed 
        after the user or other users submit/remove their votes
    */
    func refresh()
    {
        questionTextView?.text = question?["questionText"] as! String
        resultsTextView?.text = ""
        guard
            let answers = question?["answers"] as? [String],
            let votes = question?["votes"] as? [Int]
        else
        {
            Alert.displayAlert(title: Alert.ErrorMessages.Failed, message: Alert.ErrorMessages.QuestionsResultRefreshQuery, viewController: self, dismiss: false)
            return
        }
        
        for count in 0..<answers.count
        {
            resultsTextView?.text = "\(resultsTextView?.text as String!)\n\(answers[count]): \(votes[count])"
        }
    }
    
    /**
        Parse Question object currently opened

        Object Properties:

            objectId        : String            Unique Parse object ID

            clubId          : String            Unique Parse object Club ID to which the question belongs to

            createdAt       : Date              Date of creation

            updatedAt       : Date              Date last modified

            questionText    : String            Question text

            whoVotedForWhat :[String : int]     Dictionary of User IDs to vote selections
            i.e. User qpxBSid2QJ voted for answer# 2

            answers         :[String]           Array of text answers to the question
     */
    var question: PFObject?
    {
        didSet
        {
            refresh()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if following == false
        {
            submitButton.enabled = false
            refresh()
        }
        else
        {
            QuestionsQuery.checkIfVoted(questionObject: question, sender: self)
        }
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return (question?["answers"] as! [String]).count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return (question?["answers"] as! [String])[row]
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        if let label = view as? UILabel
        {
            label.adjustsFontSizeToFitWidth = true
            return label
        }
        let label = UILabel()
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = NSTextAlignment.Center
        label.text = (question?["answers"] as! [String])[row]
        return label
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedRow = row
    }
    
}
