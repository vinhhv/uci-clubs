//
//  QuestionsTableViewController.swift
//  My Clubs
//
//  Created by Vinh Vu on 9/12/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import UIKit
import Parse

class QuestionsTableViewController: UITableViewController {
    
    @IBAction func refresh(sender: UIRefreshControl?) {
        QuestionsQuery.getClubQuestions(tabBar!.clubId, sender: self, refresh: sender)
    }
    
    /**
        Parse Relation indicating which set of Question objects 
        in the Parse database belongs to the current club.
     
        This allows the application to quickly pull in the correct 
        subset of Question objects without searching through the
        entire database.
    */
    var questionsRelation: PFRelation?
    
    /**
         Array of Parse Question objects
         
         Object Properties:
         
            objectId        : String            Unique Parse object ID
     
            clubId          : String            Unique Parse object Club ID to which the question belongs to
             
            createdAt       : Date              Date of creation
             
            updatedAt       : Date              Date last modified
             
            questionText    : String            Question text
             
            whoVotedForWhat :[String : int]     Dictionary of User IDs to vote selections
                                                i.e. User qpxBSid2QJ voted for answer# 2
            
            answers         :[String]           Array of text answers to the question
     */
    var questions: [PFObject]?
    {
        didSet
        {
            self.tableView.reloadData()
            if questions!.count < 1
            {
                Alert.displayAlert(title: Alert.EmptyMessages.Empty, message: Alert.EmptyMessages.Questions, viewController: self, dismiss: false)
            }
        }
    }
    
    // The parent ClubTabBarController in which this view controller currently resides in
    var tabBar: ClubTabBarController?
    
    /**
        Following status of user to club
     
        This ensures only members of the club are able to view
        the current survey questions.
     */
    var following: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
        QuestionsQuery.getClubQuestions(tabBar!.clubId, sender: self, refresh: nil)
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let questions = questions
        else
        {
            return 0
        }
        return questions.count
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("questionCell", forIndexPath: indexPath) as! QuestionTableViewCell
        
        guard let questions = questions where questions.count > 0,
            let createdAt = questions[indexPath.row].createdAt,
            let questionText = questions[indexPath.row]["questionText"] as? String
        else
        {
            return cell
        }
        cell.dateCreated = GeneralDateFormatter.createStringFromDate(date: createdAt)
        cell.questionText = questionText
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let questions = questions where questions.count > 0,
            let identifier = segue.identifier where identifier == "ShowVotingViewController",
            let votingViewController = segue.destinationViewController as? VotingViewController,
            let questionsRelation = questionsRelation,
            let cell = sender as? QuestionTableViewCell,
            let indexPath = self.tableView.indexPathForCell(cell)
        else
        {
            return
        }
        
        let row = indexPath.row
        votingViewController.question = questions[row]
        votingViewController.questionsRelation = questionsRelation
        votingViewController.tabBar = tabBar
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
