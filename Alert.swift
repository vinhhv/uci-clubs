//
//  AlertPopUp.swift
//  My Clubs
//
//  Created by Vinh Vu on 6/26/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import UIKit

class Alert: NSObject {
    
    // MARK: Message Shortcuts
    
    static let PTAL = "Please try again later!"
    static let PCIC = "Please check your internet connection!"
    
    // MARK: Handling Error Messages
    
    /**
        Error types involving Parse data types and optionals
    
        - ParseToNativeConversionError: Error due to failure to convert from a Parse data type to a native Swift data type.
        - NativeToParseConversionError: Error due to failure to convert from a native Swift data type to a Parse data type.
        - ParseOptionalUnwrapError: Error due to failure to unwrap an optional Parse data type.
        - SwiftOptionalUnwrapError: Error due to failure to unwrap an optional Swift data type.
    */
    enum QueryError: ErrorType
    {
        case ParseToNativeConversionError
        case NativeToParseConversionError
        case ParseOptionalUnwrapError
        case SwiftOptionalUnwrapError
    }
    
    // MARK: Success Messages
    
    /**
        Various alert message shortcuts indicating a successful query.
    */
    enum SuccessMessages
    {
        // generic success message
        static let Success = "Success!"
        // successful query messages
        static let LogInQuery = "You are now logged in!"
        static let LogOutQuery = "You are now logged out!"
        static let SignUpQuery = "You are now signed up!"
        static let StartFollowingQuery = "You are now following this club!"
        static let StopFollowingQuery = "You are no longer following this club!"
    }
    
    // MARK: Failure Messages
    
    /**
        Various alert message shortcuts indicating a failed query.
    */
    enum ErrorMessages
    {
        // generic failed message
        static let Failed = "Failed!"
        // failed query messages
        static let AnnouncementsQuery = "Could not load annoucements. \(PCIC)"
        static let CheckIfVotedQuery = "Could not load your vote status. \(PCIC)"
        static let ClubsQuery = "Could not load clubs. \(PTAL)"
        static let LogInQuery = "Could not log in at this moment. \(PTAL)"
        static let LogOutQuery = "Could not log out at this moment. \(PTAL)"
        static let MembersQuery = "Could not retrieve list of members. \(PCIC)"
        static let MustBeLoggedInToFollow = "You must be logged in to follow this club!"
        static let NewsFeedQuery = "Unable to load the news feed. \(PTAL)"
        static let QuestionsQuery = "Could not load club questions. \(PCIC)"
        static let QuestionsResultRefreshQuery = "Could not refresh voting results. \(PCIC)"
        static let SaveQuestionQuery = "Could not save your vote selection. \(PCIC)"
        static let StartFollowingQuery = "Unable to follow this club at the moment. \(PTAL)"
        static let StopFollowingQuery = "Unable to unfollow this club at the moment. \(PTAL)"
        static let UserInfo = "Could not load the current user's information. \(PTAL)"
    }

    // MARK: Empty Results Messages
    
    /**
        Various alert message shortcuts indicating a successful, but empty query.
    */
    enum EmptyMessages
    {
        // generic empty message
        static let Empty = "You're shit out of luck!"
        // empty query messages
        static let Announcements = "This club currently has no announcements."
        static let NewsFeed = "No announcements available! Please login to see your announcements!"
        static let Questions = "This club currently has no polls."
    }
    
    // MARK: - Displaying Alert Functions
    
    /**
        Displays an alert to the user with the option to dismiss the current view controller in the navigation controller stack.
    
        - Parameters:
            
            - title: The title of alert message.
    
            - message: The message text.
    
            - viewController: The view controller requesting the message.
    
            - dismiss: Boolean value indicating whether acknowledging the alert will dismiss the current view controller.
    */
    static func displayAlert(title title: String, message: String, viewController: UIViewController, dismiss: Bool)
    {
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            if (dismiss == true)
            {
                viewController.dismissViewControllerAnimated(true, completion:nil)
            }
        }))
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            if let clubTabView = viewController.tabBarController as? ClubTabBarController
            {
                clubTabView.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
                viewController.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    /**
        Displays an alert to the user with the option to dismiss the current view controller in the navigation controller stack.

        - Parameters:

            - title: The title of alert message.

            - message: The message text.

            - viewController: The view controller requesting the message.

            - goToTopView: Boolean value indicating whether acknowledging the alert will pop to the root view controller.
     */
    static func displayAlert(title title: String, message: String, viewController: UIViewController, goToTopView: Bool)
    {
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            if (goToTopView == true)
            {
                viewController.navigationController?.popToRootViewControllerAnimated(true)
            }
        }))
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            if let clubTabView = viewController.tabBarController as? ClubTabBarController
            {
                clubTabView.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
                viewController.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
}
