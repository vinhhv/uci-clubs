//
//  ActivityIndicator.swift
//  My Clubs
//
//  Created by Vinh Vu on 6/27/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import UIKit

class ActivityIndicator: NSObject {
    
    static var ignoringEvents = false

    static var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    static func displayActivityIndicator(viewController viewController: UIViewController, viewMain: UIView, ignoreEvents: Bool)
    {
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            activityIndicator = UIActivityIndicatorView(frame: viewController.view.frame)
            activityIndicator.center = viewController.view.center
            activityIndicator.backgroundColor = UIColor(white: 1.0, alpha: 0.5)
            activityIndicator.hidesWhenStopped = true
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
            viewMain.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            
            if ignoreEvents
            {
                ignoringEvents = true
                UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            }
        }
    }
    
    static func stopActivityIndicator()
    {
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            activityIndicator.stopAnimating()
            if ignoringEvents
            {
                ignoringEvents = false
                UIApplication.sharedApplication().endIgnoringInteractionEvents()
            }
        }
    }
}
