//
//  Announcement.swift
//  My Clubs
//
//  Created by Vinh Vu on 8/20/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import UIKit

class Announcement: NSObject {
    var clubName:String
    var clubId:String
    var title:String 
    var info:String
    var createdDate:NSDate
    var lastUpdated:NSDate
    
    init(clubName: String, clubId: String, title: String, info: String, createdDate: NSDate, lastUpdated: NSDate)
    {
        self.clubName = clubName
        self.clubId = clubId
        self.title = String(title)
        self.info = String(info)
        self.createdDate = createdDate
        self.lastUpdated = lastUpdated
    }
    
    func updateTitle(title: String)
    {
        self.title = title
        lastUpdated = NSDate()
    }
    
    func updateInfo(info: String)
    {
        self.info = info
        lastUpdated = NSDate()
    }
    
    func getCreatedDate() -> String
    {
        return GeneralDateFormatter.createStringFromDate(date: createdDate)
    }
    
    func getLastUpdated() -> String
    {
        return GeneralDateFormatter.createStringFromDate(date: lastUpdated)
    }
}
