//
//  QuestionCell.swift
//  My Clubs
//
//  Created by Vinh Vu on 9/13/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import UIKit

class QuestionTableViewCell: UITableViewCell
{
    
    @IBOutlet weak var dateAskedLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    
    var dateCreated: String = ""
    {
        didSet
        {
            dateAskedLabel?.text = "Asked at " + dateCreated
            dateAskedLabel?.adjustsFontSizeToFitWidth = true
        }
    }
    
    var questionText: String = ""
    {
        didSet
        {
            questionLabel?.text = questionText
        }
    }
}
