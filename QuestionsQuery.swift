//
//  QuestionsQuery.swift
//  My Clubs
//
//  Created by Vinh Vu on 9/13/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import Foundation
import Parse

class QuestionsQuery
{
    
    // MARK: - General Questions Queries
    
    /**
        Fetches all questions/polls for a specific club given its unique ID.
     
        - Parameters:
     
            - clubId: The unique Parse ID given to this club.
    
            - sender: The QuestionsTableViewController that requested the query. The sender's 'questions' variable ([PFObject]) will be updated with the results.
    
            - refresh: The UIRefreshControl object which will be called to end refreshing when the query results are delivered.
    */
    static func getClubQuestions(clubId: String, sender: QuestionsTableViewController, refresh: UIRefreshControl?)
    {
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)) { () -> Void in
            let clubQuery = PFQuery(className: "Club")
            do
            {
                let club = try clubQuery.getObjectWithId(clubId)

                if let questionsRelation = club.relationForKey("questions") as PFRelation?
                {
                    sender.questionsRelation = questionsRelation
                    let queryQuestions = try questionsRelation.query()?.orderByDescending("createdAt").findObjects()
                    
                    guard let questions = queryQuestions else
                    {
                        throw Alert.QueryError.ParseOptionalUnwrapError
                    }
                    
                    for question in questions
                    {
                        if question["whoVotedForWhat"] == nil
                        {
                            question["whoVotedForWhat"] = [String:Int]()
                        }
                        try question.save()
                    }
                    dispatch_async(dispatch_get_main_queue()) {
                        sender.questions = questions
                        refresh?.endRefreshing()
                    }
                }
        
            }
            catch
            {
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    sender.questions = []
                    refresh?.endRefreshing()
                }
                Alert.displayAlert(title: Alert.ErrorMessages.Failed, message: Alert.ErrorMessages.QuestionsQuery, viewController: sender, dismiss: false)
            }
        }
    }
    
    /**
        Fetches the Parse Question object given its unique ID.
     
        - Parameters:
     
            - questionId: The unique Parse ID given to this question object.
     
            - sender: The VotingViewController that requested the query. The sender's 'question' variable (PFObject) will be updated with the result.
    */
    static func getQuestionById(questionId id: String, sender: VotingViewController)
    {
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)) { () -> Void in
            guard let relation = sender.questionsRelation else
            {
                return
            }
            do
            {
                let question = try relation.query()?.getObjectWithId(id)
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    sender.question = question
                }
            }
            catch {}
            
        }
    }
    
    // MARK: - Voting Queries
    
    /**
        The current voting status for the user.

        Status Info:

        - NoUser: No user is currently logged in: the submit vote button is disabled

        - Error: An error occurred: the submit vote button is disabled

        - NotAlreadyVoted: User is logged in and has not submitted a vote: submit vote button is enabled

        - AlreadyVoted: User is logged in and has already submitted a vote: remove vote button is enabled
    */
    enum VoteDetail
    {
        case NoUser
        case Error
        case AlreadyVoted
        case NotAlreadyVoted
    }
    
    /**
        The current voting status for the user and the vote selection chosen by the user.
     
        - voteStatus: The VoteDetail object.
     
        - votedFor: The index indicating the location of the vote selected in the array of answers.
    */
    struct VoteStatus
    {
        var voteStatus: VoteDetail
        var votedFor: Int?
    }
    
    /**
        Checks if the user has voted for a question given the Parse Question object.
     
        - Parameters:
     
            - questionObject: The Parse Question object.
     
            - sender: The VotingViewController which requested the query. The sender's 'voteStatus' variable (VoteStatus) will be updated with the result.
    */
    static func checkIfVoted(questionObject question: PFObject?, sender: VotingViewController)
    {
        
        if PFUser.currentUser() == nil
        {
            let voteStatus = VoteStatus(voteStatus: VoteDetail.NoUser, votedFor: nil)
            sender.voteStatus = voteStatus
            return
        }
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)) { () -> Void in
            var voteStatus: QuestionsQuery.VoteStatus
            
            guard
                let question = question,
                let whoVotedForWhat = question["whoVotedForWhat"] as? [String: Int],
                let user = PFUser.currentUser()
            else
            {
                Alert.displayAlert(title: Alert.ErrorMessages.Failed, message: Alert.ErrorMessages.CheckIfVotedQuery, viewController: sender, dismiss: false)
                voteStatus = VoteStatus(voteStatus: VoteDetail.Error, votedFor: nil)
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    sender.voteStatus = voteStatus
                }
                return
            }
            
            for key in whoVotedForWhat.keys
            {
                if key == user.objectId!
                {
                    voteStatus = VoteStatus(voteStatus: VoteDetail.AlreadyVoted, votedFor: whoVotedForWhat[key]!)
                    dispatch_async(dispatch_get_main_queue()) { () -> Void in
                        sender.voteStatus = voteStatus
                    }
                    return
                }
            }
            
            voteStatus = VoteStatus(voteStatus: VoteDetail.NotAlreadyVoted, votedFor: nil)
            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                sender.voteStatus = voteStatus
            }
        }
    }
    
    /**
        Updates the question's or poll's vote count given the Parse Question object and the selected vote.
     
        - Parameters:
     
            - questionObject: The Parse Question object that will be updated.
     
            - selectedRow: The index representing the vote selection.
     
            - sender: The VotingViewController which requested the query. The sender's voting information will be updated.
     
            - addVote: Boolean indicating whether the vote is to be added or removed.
    */
    static func updateVote(questionObject question: PFObject?, selectedRow: Int, sender: VotingViewController, addVote: Bool)
    {
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)) { () -> Void in
            
            guard let question = question,
                var votes = question["votes"] as? [Int],
                var whoVotedForWhat = question["whoVotedForWhat"] as? [String: Int],
                let user = PFUser.currentUser()
                else
            {
                Alert.displayAlert(title: "Failed", message: Alert.ErrorMessages.SaveQuestionQuery, viewController: sender, dismiss: false)
                return
            }
            
            if addVote
            {
                votes[selectedRow]++
                whoVotedForWhat[user.objectId!] = selectedRow
            }
            else
            {
                votes[selectedRow]--
                whoVotedForWhat.removeValueForKey(user.objectId!)
            }
            
            question["votes"] = votes
            question["whoVotedForWhat"] = whoVotedForWhat
            
            do
            {
                try question.save()
            }
            catch
            {
                Alert.displayAlert(title: "Failed", message: Alert.ErrorMessages.SaveQuestionQuery, viewController: sender, dismiss: false)
            }
            
            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                if addVote
                {
                    sender.refresh()
                    sender.alreadyVoted = true
                    sender.currentVote = selectedRow
                    sender.submitButton.setTitle("Remove", forState: .Normal)
                }
                else
                {
                    sender.refresh()
                    sender.alreadyVoted = false
                    sender.submitButton.setTitle("Submit", forState: .Normal)
                }
            }
        }
    }
    
    // MARK: - Question Creation/Deletion
    
    static func save(question: Question, clubId: String, sender: VotingViewController)
    {
        // TODO: Saving the question when Club owner/board members create a new question
    }

}