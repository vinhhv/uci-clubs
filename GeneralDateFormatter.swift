//
//  GeneralDateFormatter.swift
//  My Clubs
//
//  Created by Vinh Vu on 9/13/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import Foundation

class GeneralDateFormatter
{
    static private var dateFormatter:NSDateFormatter = NSDateFormatter()
    
    static func createStringFromDate(date date: NSDate) -> String
    {
        dateFormatter.dateFormat = "hh:mm a, MM-dd-yyyy"
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        return dateFormatter.stringFromDate(date)
    }
}