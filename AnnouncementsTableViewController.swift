//
//  AnnouncementsTableViewController.swift
//  My Clubs
//
//  Created by Vinh Vu on 9/16/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import UIKit
import Parse

class AnnouncementsTableViewController: UITableViewController {
    
    @IBAction func refresh(sender: UIRefreshControl?) {
        AnnouncementsQuery.getAnnouncements(clubId: (tabBar as ClubTabBarController!).clubId, sender: self, refresh: sender)
    }
    
    /**
         Array of Parse Announcement objects
         
         Object Properties:
         
             objectId    : String            Unique Parse object ID
             
             createdAt   : Date              Date of creation
             
             updatedAt   : Date              Date last modified
             
             title       : String            Title of announcement
             
             subTitle    : String            Subtitle supplementing title
             
             info        : String            Basic announcement information
     */
    var announcements: [PFObject]?
    {
        didSet
        {
            tableView.reloadData()
            if announcements!.count < 1
            {
                Alert.displayAlert(title: Alert.EmptyMessages.Empty, message: Alert.EmptyMessages.Announcements, viewController: self, dismiss: false)
            }
        }
    }
    
    // The parent ClubTabBarController in which this view controller currently resides in
    var tabBar: ClubTabBarController?
    
    /**
        Following status of user to club

        This may be necessary in case the club requires that the user be following them
        in order to view their announcements
    */
    var following: Bool?
    
    var selectedIndexPath: NSIndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
        AnnouncementsQuery.getAnnouncements(clubId: (tabBar as ClubTabBarController!).clubId, sender: self, refresh: nil)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let announcements = announcements
        else
        {
            return 0
        }
        return announcements.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCellWithIdentifier("announcementCell") as? AnnouncementTableViewCell else
        {
            let cell = AnnouncementTableViewCell()
            return cell
        }
        
        cell.title.adjustsFontSizeToFitWidth = true
        cell.infoLabel.sizeToFit()
        
        guard let announcements = announcements where announcements.count > 0 else
        {
            return cell
        }
            
        cell.title.text = announcements[indexPath.row]["title"] as? String
        cell.subTitle.text = announcements[indexPath.row]["subTitle"] as? String
        cell.createdDate.text = "Created: " + GeneralDateFormatter.createStringFromDate(date: announcements[indexPath.row].createdAt as NSDate!)
        cell.updatedDate.text = "Updated: " + GeneralDateFormatter.createStringFromDate(date: announcements[indexPath.row].updatedAt as NSDate!)
        cell.infoLabel.text = announcements[indexPath.row]["info"] as? String
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let previousIndexPath = selectedIndexPath
        if indexPath == selectedIndexPath
        {
            selectedIndexPath = nil
        }
        else
        {
            selectedIndexPath = indexPath
        }
        
        var indexPaths: [NSIndexPath] = []
        if let previous = previousIndexPath
        {
            indexPaths += [previous]
        }
        if let current = selectedIndexPath
        {
            indexPaths += [current]
        }
        if indexPaths.count > 0
        {
            tableView.reloadRowsAtIndexPaths(indexPaths, withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = cell as? AnnouncementTableViewCell
        {
            cell.checkHeight()
        }
    }
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath == selectedIndexPath && announcements!.count > 0
        {
            return AnnouncementTableViewCell.RowHeight.Selected
        }
        return AnnouncementTableViewCell.RowHeight.Unselected
    }
}
