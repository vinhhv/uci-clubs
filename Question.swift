//
//  Question.swift
//  My Clubs
//
//  Created by Vinh Vu on 9/13/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import Foundation
import Parse

class Question
{
    var questionText: String
    var questionId: String
    var clubId: String
    var answers: [String]
    var numberOfVotes: [Int]
    var locked: Bool
    var dateAsked: String
    var whoVotedForWhat: [String:Int]
    
    // For retrieving an already existing Parse Question object
    init(questionText: String, questionId: String, dateAsked: NSDate, clubId: String, listOfAnswers answers: [String], numberOfVotes: [Int], whoVotedForWhat: [String:Int])
    {
        self.questionText = questionText
        self.questionId = questionId
        self.clubId = clubId
        self.answers = answers
        self.numberOfVotes = numberOfVotes
        self.locked = false
        self.dateAsked = GeneralDateFormatter.createStringFromDate(date: dateAsked)
        self.whoVotedForWhat  = whoVotedForWhat
    }
    
    // For creating a new question
    init(questionText: String, clubId: String, listOfAnswers answers: [String])
    {
        self.questionText = questionText
        self.questionId = ""
        self.clubId = clubId
        self.answers = answers
        self.numberOfVotes = [Int]()
        self.locked = false
        self.dateAsked = GeneralDateFormatter.createStringFromDate(date: NSDate())
        self.whoVotedForWhat = [String:Int]()
        for _ in 0...(answers.count - 1)
        {
            numberOfVotes.append(0)
        }
        
        // TODO: This method must call Parse and create the object in the database in order to successfully create the question locally
    }
    
    // Default initializer if parameters are not known yet
    init()
    {
        questionText = ""
        questionId = ""
        clubId = ""
        answers = [String]()
        numberOfVotes = [Int]()
        locked = false
        dateAsked = ""
        whoVotedForWhat = [String:Int]()
    }
    
    func addVote(voteIndex index: Int, amountOfVotes votes: Int)
    {
        numberOfVotes[index] += votes
    }
    
    func removeVote(voteIndex index: Int, amountOfVotes votes: Int)
    {
        numberOfVotes[index] -= votes
    }
    
    func removeFromDatabase()
    {
        // TODO: Create query to remove object from Parse database
    }
    
    func lockInVoting()
    {
        // TODO: This function will lock in voting and display the final results
    }
}