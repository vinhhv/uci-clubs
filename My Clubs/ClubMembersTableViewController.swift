//
//  ClubMembersTableViewController.swift
//  My Clubs
//
//  Created by Vinh Vu on 8/17/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import UIKit

class ClubMembersTableViewController: UITableViewController {
    
    // Array of club member names
    var clubMembers:[String] = [String]()
    {
        didSet
        {
            self.tableView.reloadData()
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "memberCell")
        
        cell.textLabel?.text = self.clubMembers[indexPath.row]
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        
        return cell
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clubMembers.count
    }

}