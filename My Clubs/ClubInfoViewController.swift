//
//  ClubInfoViewController.swift
//  My Clubs
//
//  Created by Vinh Vu on 6/14/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import UIKit
import Parse


class ClubInfoViewController: UIViewController {
    
    @IBOutlet var clubName: UILabel!
    @IBOutlet var clubInfo: UILabel!
    
    // The parent ClubTabBarController in which this view controller currently resides in
    var tabBar: ClubTabBarController?
    
    // Following status of user to club
    // Any changes made will resonate with the parent ClubTabBarController
    var following: Bool?
    {
        didSet
        {
            if tabBar!.following != following
            {
                tabBar!.following = following
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backButton
        
        
        clubName.text = tabBar!.clubName
        
        clubName.adjustsFontSizeToFitWidth = true
        clubInfo.adjustsFontSizeToFitWidth = true

        clubInfo.text = "Multiple lines test test test test test test test test test test test test test"
    }
}

