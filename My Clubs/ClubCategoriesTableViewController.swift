//
//  ClubCategoriesTableViewController.swift
//  Advanced Segues
//
//  Created by Vinh Vu on 6/19/15.
//  Copyright (c) 2015 Appfish. All rights reserved.
//

import UIKit
import Parse


class ClubCategoriesTableViewController: UITableViewController, UINavigationControllerDelegate {
    
    // Array of categories by name
    var clubCategories = [String]()
    {
        didSet
        {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.delegate = self
        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backButton
        ClubsQuery.getClubCategories(self)
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clubCategories.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "categoryCell")
        cell.textLabel?.text = clubCategories[indexPath.row]
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("ShowClubsTableViewController", sender: indexPath)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue,
        sender: AnyObject?) {
            if segue.identifier == "ShowClubsTableViewController"
            {
                let clubsTableViewController = segue.destinationViewController as! ClubsTableViewController
                let myIndexPath = sender as! NSIndexPath
                let row = myIndexPath.row
                clubsTableViewController.navigationItem.title = clubCategories[row]
                ClubsQuery.getClubsByCategory(clubCategory: clubCategories[row], sender: clubsTableViewController)
            }
    }
}
