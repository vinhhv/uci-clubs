//
//  LoginViewController.swift
//  My Clubs
//
//  Created by Vinh Vu on 6/26/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import UIKit
import Parse

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet var enteredUsername: UITextField!
    @IBOutlet var enteredPassword: UITextField!
    @IBOutlet var formButton: UIButton!
    @IBOutlet var memberText: UILabel!
    @IBOutlet var memberButton: UIButton!
    
    // Value indicating whether the page displayed is to sign up or to log in, default is log in
    var signupActive = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: view.window)
        
        self.usernameTextField.delegate = self
        self.passwordTextField.delegate = self
        
        
        formButton.setTitle("Log In", forState: UIControlState.Normal)
        
        memberText.text = "Not a member yet?"
        memberButton.setTitle("Sign Up", forState: UIControlState.Normal)
    }

    /**
        Depending on the active form, Log In or Sign Up
     
        Run the appropriate query
    */
    @IBAction func formButtonPressed(sender: AnyObject) {
        if enteredUsername.text == "" || enteredPassword.text == ""
        {
            Alert.displayAlert(title: "Empty Field(s)", message: "Please enter a username and password", viewController: self, dismiss: true)
        }
        else
        {
            if signupActive == true
            {
                UserQuery.signUp(sender: self)
            }
            else
            {
                UserQuery.logIn(sender: self)
            }
        }
    }
    
    /**
        Switches the forms of the page
     
        If signupActive is true, switch the form to allow the Log In option.
        Else, switch the form to allow the Sign Up option.
    */
    @IBAction func memberButtonPressed(sender: AnyObject) {
        if signupActive == true
        {
            formButton.setTitle("Log In", forState: UIControlState.Normal)
            
            memberText.text = "Not a member yet?"
            memberButton.setTitle("Sign Up", forState: UIControlState.Normal)
            
            signupActive = false
        }
        else
        {
            formButton.setTitle("Sign Up", forState: UIControlState.Normal)
            
            memberText.text = "Already a member?"
            memberButton.setTitle("Log In", forState: UIControlState.Normal)
            
            signupActive = true
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func keyboardWillShow(notification: NSNotification)
    {
        print("\n\nNotification:\n \(notification.userInfo)\n\n")
    }
    
}
