//
//  AnnouncementTableViewCell.swift
//  My Clubs
//
//  Created by Vinh Vu on 9/2/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import UIKit

class AnnouncementTableViewCell: UITableViewCell
{
    
    @IBOutlet var clubName: UILabel!
    @IBOutlet var createdDate: UILabel!
    @IBOutlet var updatedDate: UILabel!
    @IBOutlet var announcementInfo: UILabel!
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    struct RowHeight
    {
        static let Selected: CGFloat = 240.0
        static let Unselected: CGFloat = 100.0
        static let SelectedNewsFeed: CGFloat = 284.0
        static let UnselectedNewsFeed: CGFloat = 130.0
    }
    
    func checkHeight()
    {
        infoLabel?.hidden = (frame.size.height <= RowHeight.Unselected)
    }
    
    func checkHeightNewsFeed()
    {
        infoLabel?.hidden = (frame.size.height <= RowHeight.UnselectedNewsFeed)
    }
}
