//
//  ClubSubListNavigationViewController.swift
//  My Clubs
//
//  Created by Vinh Vu on 6/19/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import UIKit

class ClubListNavigation: UINavigationController {
    
    var updatedTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = updatedTitle
        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
