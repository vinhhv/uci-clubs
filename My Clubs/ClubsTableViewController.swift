//
//  ClubsTableViewController.swift
//  My Clubs
//
//  Created by Vinh Vu on 6/19/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import UIKit
import Parse

class ClubsTableViewController: UITableViewController, UINavigationControllerDelegate {
    
    /**
        Dictionary for clubs formatted [Club ID : (Club Name, Club ID)]
     
        Club ID - Unique Parse class object ID
        Club Name - Club's title
    */
    var clubsList = [String: (String, String)]()
    {
        didSet
        {
            sortedClubsList = Array(clubsList.values)
            // Sort tuples by 1st element, then by 2nd element if equal
            sortedClubsList.sortInPlace{ $0.0 == $1.0 ? $0.1 < $1.1 : $0.0 < $1.0}
        }
    }
    
    // Array of club pairs formatted (Club Name, Club ID)
    var sortedClubsList = [(String,String)]()
    {
        didSet
        {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.delegate = self
        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backButton
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clubsList.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "clubCell")
        
        cell.textLabel?.text = self.sortedClubsList[indexPath.row].0
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("ShowClubTabBarController", sender: indexPath)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowClubTabBarController" {
            let clubTabbedView = segue.destinationViewController as! ClubTabBarController
            let myIndexPath = sender as! NSIndexPath
            let selectedRow = myIndexPath.row
            
            clubTabbedView.clubName = self.sortedClubsList[selectedRow].0
            clubTabbedView.clubId = self.sortedClubsList[selectedRow].1
            UserQuery.isFollowing(clubId: sortedClubsList[selectedRow].1, sender: clubTabbedView)

        }
    }
}
