//
//  SplashScreen.swift
//  UCI Clubs
//
//  Created by Vinh Vu on 9/30/15.
//  Copyright © 2015 Vinguh Industries. All rights reserved.
//

import UIKit

class SplashScreen: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        self.performSegueWithIdentifier("ShowNewsFeed", sender: self)
    }
}
