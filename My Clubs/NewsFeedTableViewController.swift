//
//  NewsFeedTableViewController.swift
//  My Clubs
//
//  Created by Vinh Vu on 8/20/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import UIKit
import Parse

class NewsFeedTableViewController: UITableViewController {

    @IBOutlet weak var loginButton: UIButton!
    @IBAction func refresh(sender: UIRefreshControl?) {
        checkNewsFeed(sender: sender)
    }
    
    @IBAction func loginPressed(sender: UIButton) {
        guard var someoneIsLoggedIn = someoneIsLoggedIn else
        {
            return
        }
        if someoneIsLoggedIn
        {
            UserQuery.logOut(sender: self)
            loginButton.setTitle("Log In", forState: .Normal)
            someoneIsLoggedIn = false
        }
        else
        {
            performSegueWithIdentifier("ShowLoginViewController", sender: self)
        }
    }
    
    // List of active clubs (by Club ID) being followed by the user
    var followingList: [String]?
    
    /**
        Array of Parse Announcement objects
     
        Object Properties:
     
            objectId    : String            Unique Parse object ID
     
            createdAt   : Date              Date of creation
     
            updatedAt   : Date              Date last modified
     
            title       : String            Title of announcement
     
            subTitle    : String            Subtitle supplementing title
     
            info        : String            Basic announcement information
     */
    var announcements: [PFObject]?
    {
        didSet
        {
            self.tableView.reloadData()
        }
    }
    
    // Status of user being logged in
    var someoneIsLoggedIn: Bool?
    {
        didSet
        {
            self.tableView.reloadData()
        }
    }
    var selectedIndexPath: NSIndexPath?
    
    // Checks login status, setting the log in/out button title appropriately
    func checkLoginStatus()
    {
        someoneIsLoggedIn = UserQuery.isLoggedIn()
        guard let someoneIsLoggedIn = someoneIsLoggedIn else
        {
            return
        }
        if someoneIsLoggedIn
        {
            loginButton.setTitle("Log Out", forState: .Normal)
        }
        else
        {
            loginButton.setTitle("Log In", forState: .Normal)
        }
    }
    
    
    // Checks if news feed is to be queried depending on login status
    func checkNewsFeed(sender sender: UIRefreshControl?)
    {
        if someoneIsLoggedIn == true
        {
            AnnouncementsQuery.getUserNewsFeed(sender: self, refresh: sender)
        }
        else if someoneIsLoggedIn == false
        {
            Alert.displayAlert(title: Alert.EmptyMessages.Empty, message: Alert.EmptyMessages.NewsFeed, viewController: self, dismiss: false)
            refreshControl?.endRefreshing()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        checkLoginStatus()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backButton
        
        checkLoginStatus()
        checkNewsFeed(sender: nil)
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let announcements = announcements else
        {
            return 0
        }
        return announcements.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier("newsFeedCell") as? AnnouncementTableViewCell else
        {
            let cell = AnnouncementTableViewCell()
            return cell
        }
        
        guard let announcements = self.announcements where announcements.count > 0 else
        {
            return cell
        }
    
        
        cell.title?.adjustsFontSizeToFitWidth = true
        cell.infoLabel?.sizeToFit()
        
        cell.clubName?.text = announcements[indexPath.row]["clubName"] as? String
        cell.clubName?.adjustsFontSizeToFitWidth = true
        cell.title?.text = announcements[indexPath.row]["title"] as? String
        cell.subTitle?.text = announcements[indexPath.row]["subTitle"] as? String
        cell.createdDate?.text = "Created: " + GeneralDateFormatter.createStringFromDate(date: announcements[indexPath.row].createdAt as NSDate!)
        cell.updatedDate?.text = "Updated: " + GeneralDateFormatter.createStringFromDate(date: announcements[indexPath.row].updatedAt as NSDate!)
        cell.infoLabel?.text = announcements[indexPath.row]["info"] as? String

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let previousIndexPath = selectedIndexPath
        if indexPath == selectedIndexPath
        {
            selectedIndexPath = nil
        }
        else
        {
            selectedIndexPath = indexPath
        }
        
        var indexPaths: [NSIndexPath] = []
        if let previous = previousIndexPath
        {
            indexPaths += [previous]
        }
        if let current = selectedIndexPath
        {
            indexPaths += [current]
        }
        if indexPaths.count > 0
        {
            tableView.reloadRowsAtIndexPaths(indexPaths, withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = cell as? AnnouncementTableViewCell
        {
            cell.checkHeightNewsFeed()
        }
    }
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath == selectedIndexPath && announcements!.count > 0
        {
            return AnnouncementTableViewCell.RowHeight.SelectedNewsFeed
        }
        return AnnouncementTableViewCell.RowHeight.UnselectedNewsFeed
    }


}
