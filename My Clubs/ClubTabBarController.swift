//
//  ClubTabBarController.swift
//  My Clubs
//
//  Created by Vinh Vu on 9/9/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import UIKit

class ClubTabBarController: UITabBarController {
    
    var followAction: UIAlertAction?
    var unfollowAction: UIAlertAction?
    var cancelAction: UIAlertAction?
    var clubName = ""
    var clubId = ""
    var settings: UIAlertController?
    
    // Following status of user to club
    var following: Bool?
    {
        didSet
        {
            refresh()
        }
    }
    
    // Shows the settings for each club
    func showSettings()
    {
        if let settings = settings
        {
            presentViewController(settings, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let settingsButton = UIBarButtonItem(title: "Settings", style: .Plain, target: self, action: "showSettings")
        self.navigationItem.rightBarButtonItem = settingsButton

        refresh()
    }
    
    // Adds selected club to following list
    private func startFollowing()
    {
        if let view = self.viewControllers![0] as? ClubInfoViewController
        {
            UserQuery.changeFollowStatus(clubId: clubId, startFollowing: true, sender: view)
        }
    }
    
    // Removes selected club from following list
    private func stopFollowing()
    {
        if let view = self.viewControllers![0] as? ClubInfoViewController
        {
            UserQuery.changeFollowStatus(clubId: clubId, startFollowing: false, sender: view)
        }
    }
    
    // Shows list of members in club
    private func showMembers()
    {
        performSegueWithIdentifier("Show Members", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "Show Members"
        {
            let clubMembersList = segue.destinationViewController as! ClubMembersTableViewController
            ClubsQuery.getMembers(clubId: clubId, sender: clubMembersList)
        }
    }
    
    // Refreshes button titles in settings after follow/unfollow is pressed
    private func refresh()
    {
        if let view = self.viewControllers![0] as? ClubInfoViewController
        {
            view.tabBar = self
            view.following = following
        }
        
        if let view = self.viewControllers![1] as? AnnouncementsTableViewController
        {
            view.tabBar = self
            view.following = following
        }
        
        if let view = self.viewControllers![2] as? QuestionsTableViewController
        {
            view.tabBar = self
            view.following = following
        }
        
        guard let following = following else
        {
            return
        }
        
        if following
        {
            settings = UIAlertController(title: clubName, message: "Settings", preferredStyle: .ActionSheet)
            settings?.addAction(UIAlertAction(title: "Unfollow", style: .Default, handler: { (action: UIAlertAction) -> Void in
                self.stopFollowing()
            }))
            settings?.addAction(UIAlertAction(title: "View Members", style: .Default, handler: { (action: UIAlertAction) -> Void in
                self.showMembers()
            }))
            settings?.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        }
        else
        {
            settings = UIAlertController(title: clubName, message: "Settings", preferredStyle: .ActionSheet)
            settings?.addAction(UIAlertAction(title: "Follow", style: .Default, handler: { (action: UIAlertAction) -> Void in
                self.startFollowing()
            }))
            settings?.addAction(UIAlertAction(title: "View Members", style: .Default, handler: { (action: UIAlertAction) -> Void in
                self.showMembers()
            }))
            settings?.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        }

    }
}
