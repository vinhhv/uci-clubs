# UCI Clubs
iOS application for UCI students to use in order to keep up to date with announcements and events of their favorite clubs! Uses Parse (parse.com) as main means for data storage.

# How to set up
In order for the application to fully function, you will need the API key, please contact me if you are working on the project and are a part of UCI App Development team. 
