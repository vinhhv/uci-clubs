//
//  UserQuery.swift
//  My Clubs
//
//  Created by Vinh Vu on 9/10/15.
//  Copyright (c) 2015 Vinguh Industries. All rights reserved.
//

import Foundation
import Parse

class UserQuery
{
    
    // MARK: - Login Queries
    
    /**
        Checks if a user is currently logged in.

        - Returns: A boolean value representing the user is either logged in (true) or logged out (false)
    */
    static func isLoggedIn() -> Bool
    {
        if PFUser.currentUser() != nil
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    /**
        Signs up the user with the credentials provided by the LoginViewController.
     
        - Parameters:
            
            - sender: The LoginViewController which requested the query and contains the credentials to sign up.
    */
    static func signUp(sender sender: LoginViewController)
    {
        let user = PFUser()
        user.username = sender.enteredUsername.text
        user.password = sender.enteredPassword.text
        // Creates an empty member-of list
        user.setObject([], forKey: "isMemberOf")
        
        var errorMessage:String = "Please try again later!"
        
        user.signUpInBackgroundWithBlock({ (success, error) -> Void in
            if error == nil
            {
                sender.enteredUsername.text = ""
                sender.enteredPassword.text = ""
                Alert.displayAlert(title: Alert.SuccessMessages.Success, message: Alert.SuccessMessages.SignUpQuery, viewController: sender, goToTopView: true)
            }
            else
            {
                sender.enteredPassword.text = ""
                if let errorString = error!.userInfo["error"] as? String
                {
                    errorMessage = errorString
                }
                Alert.displayAlert(title: Alert.ErrorMessages.Failed, message: errorMessage, viewController: sender, dismiss: true)
            }
        })
    }
    
    /**
        Logs in the user with the credentials provided by the LoginViewController.
     
        - Parameters:

            - sender: The LoginViewController which requested the query and contains the credentials to log in.
    */
    static func logIn(sender sender: LoginViewController)
    {
        PFUser.logInWithUsernameInBackground(sender.enteredUsername.text!, password: sender.enteredPassword.text!, block : { (user:PFUser?, error:NSError?) -> Void in
            
            var errorMessage = "Please try again later!"
            if user != nil
            {
                sender.enteredUsername.text = ""
                sender.enteredPassword.text = ""
                
                Alert.displayAlert(title: Alert.SuccessMessages.Success, message: Alert.SuccessMessages.LogInQuery, viewController: sender, goToTopView: true)
            }
            else
            {
                sender.enteredPassword.text = ""
                if let errorString = error!.userInfo["error"] as? String
                {
                    errorMessage = errorString
                }
                Alert.displayAlert(title: Alert.ErrorMessages.Failed, message: errorMessage, viewController: sender, dismiss: true)
            }
        })
    }
    
    /**
        Logs out the user and returns the user back to the login page.

        - Parameters:

            - sender: The NewsFeedTableViewController which requested the query. On success, the view will be segued to the login page.
     */
    static func logOut(sender sender: NewsFeedTableViewController)
    {
        PFUser.logOutInBackgroundWithBlock { (error) -> Void in
            if error == nil
            {
                Alert.displayAlert(title: Alert.SuccessMessages.Success, message: Alert.SuccessMessages.LogOutQuery, viewController: sender, dismiss: true)
                sender.performSegueWithIdentifier("showLoginPage", sender: sender)
            }
            else
            {
                Alert.displayAlert(title: Alert.ErrorMessages.Failed, message: Alert.ErrorMessages.LogOutQuery, viewController: sender, dismiss: true)
            }
        }
    }
    
    // MARK: - Follow/Unfollow Queries
    
    /**
        Changes the user's following status for the requested club, given its unique ID.
    
        - Parameters:
            
            - clubId: The unique Parse ID given to this club.
    
            - startFollowing: Boolean indicating whether the request is to begin or to stop following the club.
    
            - sender: The ClubInfoViewController which requested the query. The sender's follow/unfollow button title will update accordingly.
    */
    static func changeFollowStatus(clubId clubId: String, startFollowing: Bool, sender: ClubInfoViewController)
    {
        if !UserQuery.isLoggedIn()
        {
            Alert.displayAlert(title: "Not logged in!", message: Alert.ErrorMessages.MustBeLoggedInToFollow, viewController: sender, dismiss: false)
            return
        }
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)) { () -> Void in
            guard let user = PFUser.currentUser() else
            {
                Alert.displayAlert(title: Alert.ErrorMessages.Failed, message: Alert.ErrorMessages.UserInfo, viewController: sender, dismiss: false)
                return
            }
            do
            {
                let club = try PFQuery.getObjectOfClass("Club", objectId: clubId)
                
                let membersRelation = club.relationForKey("members")
                let clubsRelation = user.relationForKey("clubs")
                
                if ( startFollowing )
                {
                    clubsRelation.addObject(club)
                    membersRelation.addObject(user)
                }
                else
                {
                    clubsRelation.removeObject(club)
                    membersRelation.removeObject(user)
                }
                
                try user.save()
                try club.save()
                
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    if ( startFollowing )
                    {
                        sender.following = true
                        Alert.displayAlert(title: Alert.SuccessMessages.Success, message: Alert.SuccessMessages.StartFollowingQuery, viewController: sender, dismiss: false)
                    }
                    else
                    {
                        sender.following = false
                        Alert.displayAlert(title: Alert.SuccessMessages.Success, message: Alert.SuccessMessages.StopFollowingQuery, viewController: sender, dismiss: false)
                    }
                }
                
            }
            catch
            {
                if ( startFollowing )
                {
                    Alert.displayAlert(title: Alert.ErrorMessages.Failed, message: Alert.ErrorMessages.StartFollowingQuery, viewController: sender, dismiss: false)
                }
                else
                {
                    Alert.displayAlert(title: Alert.ErrorMessages.Failed, message: Alert.ErrorMessages.StopFollowingQuery, viewController: sender, dismiss: false)
                }
            }
        }
    }
    
    /**
        Checks if the current user is following a club given its unique club ID.

        - Parameters:

            - clubId: The unique Parse ID given to this club.

            - sender: The ClubTabBarController which requested the query. The sender's 'following' variable (Bool) will be updated to the result.
    */
    static func isFollowing(clubId clubId: String, sender: ClubTabBarController)
    {
        if !UserQuery.isLoggedIn()
        {
            sender.following = false
            return
        }
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)) { () -> Void in
            do
            {
                let club = try PFQuery.getObjectOfClass("Club", objectId: clubId)
                let membersRelation = club.relationForKey("members")
                let membersQuery = try membersRelation.query()?.findObjects()
                
                guard
                    let user = PFUser.currentUser(),
                    let members = membersQuery as? [PFUser]
                else
                {
                    throw Alert.QueryError.ParseOptionalUnwrapError
                }
                
                for member in members
                {
                    if member.username == user.username
                    {
                        dispatch_async(dispatch_get_main_queue()) { () -> Void in
                            sender.following = true
                        }
                        return
                    }
                }
                
            }
            catch {}
            
            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                sender.following = false
            }
        }
    }
}